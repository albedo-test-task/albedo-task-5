@extends('layouts.app')

@section('content')

    <div id="members-table">
        <h1>Conference members</h1>

        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">Photo</th>
                <th scope="col">Name</th>
                <th scope="col">Report subject</th>
                <th scope="col">E-mail</th>
                @auth
                    <th scope="col"></th>
                @endauth
            </tr>
            </thead>
            <tbody>
            <?php foreach($members as $member): ?>
            <tr id="member-{{ $member->id }}">
                <td><img src="<?= $member->photo ?>" alt="Photo" width="50"></td>
                <td><?= $member->first_name ?> <?= $member->last_name ?></td>
                <td><?= $member->report_subject ?></td>
                <td><a href="mailto:<?= $member->email ?>"><?= $member->email ?></a></td>
                @auth
                    <td>
                        <i
                            @if($member->status == 1)
                                class="fa fa-eye"
                            @else
                                class="far fa-eye-slash"
                            @endif
                            aria-hidden="true"
                            id='status-{{ $member->id }}'
                            @click="changeStatus('{{ $member->id }}', 'status-{{ $member->id }}')"
                        ></i>

                        <a href="/admin/members/edit/{{ $member->id }}" title="Edit">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                        <i class="fa fa-trash" aria-hidden="true"
                           @click="deleteMember('{{ $member->id }}')"
                        ></i>
                    </td>
                @endauth
            </tr>

            <?php endforeach ?>
            </tbody>
        </table>
    </div>
@endsection
