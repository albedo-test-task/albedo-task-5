<div class="mb-3">
    <label for="company">Company</label>
    <input type="text" name="company" class="form-control" id="company"
           v-model="formFields.company.value"
           @blur="validate('company', ['string'])">
    <div class="validation-error" id="company-error">
        @{{ formFields.company.errorText }}
    </div>
</div>
<div class="mb-3">
    <label for="position">Position</label>
    <input type="text" name="position" class="form-control" id="position"
           v-model="formFields.position.value"
           @blur="validate('position', ['string'])">
    <div class="validation-error" id="position-error">
        @{{ formFields.position.errorText }}
    </div>
</div>
<div class="mb-3">
    <label for="about_me">About me</label>
    <textarea name="about_me" rows="5" class="form-control" id="about_me"
              v-model="formFields.aboutMe.value"
    ></textarea>
    <div class="validation-error" id="about_me-error">
        @{{ formFields.aboutMe.errorText }}
    </div>
</div>
<div class="mb-3">
    <label for="photo">Photo</label>
    <input type="file" name="photo" class="form-control" id="photo"
           accept=".jpg, .jpeg, .png">
    <div class="validation-error" id="photo-error">
        @{{ formFields.photo.errorText }}
    </div>
</div>

