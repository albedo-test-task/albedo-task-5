@extends('layouts.app')

@section('content')

    <h1>Edit member date</h1>

    <div class="container" id="form-block">
        <div class="row justify-content-center">
            <div class="col-md-10 col-lg-8">
                <div id="app">
                    <div id="success-message" class="alert alert-success d-none" role="alert">
                        Data updated successfully
                    </div>
                    <form id="update-member" name="update-member">
                        <div class="tab-content py-4">
                            @include('member-form.step1')
                            @include('member-form.step2')
                            <input type="hidden" id="id" name="id" value="{{ $id }}">
                        </div>

                        <div class="row justify-content-between">
                            <div class="col-auto">
                                <button type="submit" class="btn btn-primary" data-enchanter="save"
                                        id="save"
                                        @click="editData">
                                    Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
