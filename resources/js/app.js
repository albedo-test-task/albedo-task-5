/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import $ from 'jquery';
window.$ = window.jQuery = $;

import {createApp} from 'vue'

// flatpickr
require("flatpickr");
import flatpickr from "flatpickr";

const app = createApp({
    data() {
        return {
            formFields: {
                firstName: {
                    value: '',
                    errorText: null
                },
                lastName: {
                    value: '',
                    errorText: null
                },
                birthday: {
                    value: '',
                    errorText: null
                },
                reportSubject: {
                    value: '',
                    errorText: null
                },
                country: {
                    value: '',
                    errorText: null
                },
                phone: {
                    value: '',
                    errorText: null
                },
                email: {
                    value: '',
                    errorText: null
                },
                company: {
                    value: '',
                    errorText: null
                },
                position: {
                    value: '',
                    errorText: null
                },
                photo: {
                    value: '',
                    errorText: null
                },
                aboutMe: {
                    value: '',
                    errorText: null
                }
            },
            step: 1,
            memberId: null
        }
    },
    methods: {
        validate: function (fieldName, validateRules) {
            let fieldValue = this.$data['formFields'][fieldName]['value'];
            let validateError = false;

            $.each(validateRules, function (index, value) {
                if (validateError != false) {
                    return false;
                }

                let rule = value.split(":");
                validateError = app[rule[0]](fieldValue, rule[1]);
            });

            if (validateError != false) {
                this.$data['formFields'][fieldName]['errorText'] = validateError;
            } else {
                this.$data['formFields'][fieldName]['errorText'] = null;
            }
        },
        required: function (value) {
            if (value == '' || !value) {
                return "Field is required";
            }

            return false;
        },
        string: function (value) {
            if (value.length > 256) {
                return "The field length cannot be more than 256 characters";
            }

            return false;
        },
        date: function (value) {
            const regex = /^\d{4}-\d{2}-\d{2}$/;
            if (value.match(regex) === null) {
                return "Date format has to be: yyyy-mm-dd";
            }

            let date = new Date();
            let timestamp = date.getTime();

            let userDate = new Date(value.split("-"));
            let userTimestamp = userDate.getTime();

            if (typeof timestamp !== 'number' || Number.isNaN(timestamp)) {
                return "Date format has to be: yyyy-mm-dd";
            }

            if (timestamp < userTimestamp) {
                return "You can't choose a future date";
            }

            return false;
        },
        fio: function (value) {
            const regex = /^\D+$/;
            if (value.match(regex) === null) {
                return "This field does not allow numeric characters";
            }

            return false;
        },
        phone: function (value) {
            const regex = /^\+\d\s\(\d{3}\)\s\d{3}\-\d{4}$/;
            if (value.match(regex) === null) {
                return "Invalid phone number format has to be: +1 (111) 111-1111";
            }

            return false;
        },
        email: function (value) {
            const regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (value.match(regex) === null) {
                return "Invalid E-mail";
            }

            app.validateOnServer('email', value);

            return false;
        },
        sentData: function (e) {
            if (e) e.preventDefault();
            let memberForm = document.getElementById('registration');

            $.ajax({
                type: 'POST',
                url: '/member/create',
                enctype: 'multipart/form-data',
                data: new FormData(memberForm),
                contentType: false,
                cache: false,
                processData: false,
                success: function (response) {
                    if (response === true) {
                        $("#step" + app.step + ", #nav-tab, #finish, #previous").addClass("d-none");
                        $("#after-form-block").removeClass("d-none");
                        localStorage.clear();
                    } else {
                        var self = app;
                        $.each(response['validateErrors'], function (fieldName, fieldValue) {
                            fieldName = app.prepareKey(fieldName);

                            self.$data['formFields'][fieldName]['errorText'] = fieldValue[0];
                        });
                    }
                }
            });

            return false;
        },
        editData: function (e) {
            if (e) e.preventDefault();
            let memberForm = document.getElementById('update-member');
            let memberId = $("#id").val();

            $.ajax({
                type: 'POST',
                url: '/admin/member/update/' + memberId,
                enctype: 'multipart/form-data',
                data: new FormData(memberForm),
                contentType: false,
                cache: false,
                processData: false,
                success: function (response) {
                    if (response === true) {
                        $("#success-message").removeClass("d-none");
                        document.getElementById("success-message").scrollIntoView();
                    } else {
                        var self = app;
                        $.each(response['validateErrors'], function (fieldName, fieldValue) {
                            fieldName = app.prepareKey(fieldName);

                            self.$data['formFields'][fieldName]['errorText'] = fieldValue[0];
                        });
                    }
                }
            });

            return false;
        },
        nextClick: function () {
            let errorsOnStep = false;

            $("#step" + this.step + " input, #step" + this.step + " select").each(function () {
                app.validateOnServer($(this).attr("id"), $(this).val());
            });

            setTimeout(function () {
                $("#step" + app.step + " input").each(function () {
                    let fieldName = app.prepareKey($(this).attr("id"));
                    if (app.$data['formFields'][fieldName]['errorText'] !== null) {
                        errorsOnStep = true;
                    }
                });

                if (!errorsOnStep) {
                    $("#step" + app.step).addClass("d-none");
                    $("#step" + app.step + "-tab").removeClass("active");
                    app.step++;
                    $("#step" + app.step).removeClass("d-none");
                    $("#step" + app.step + "-tab").addClass("active");
                }
            }, 500);

        },
        previousClick: function () {
            $("#step" + app.step).addClass("d-none");
            $("#step" + app.step + "-tab").removeClass("active");
            app.step--;
            $("#step" + app.step).removeClass("d-none");
            $("#step" + app.step + "-tab").addClass("active");
        },
        validateOnServer: function (fieldName, fieldValue) {
            let data = {};
            data[fieldName] = fieldValue;

            $.ajax({
                type: 'POST',
                url: '/check/' + fieldName,
                enctype: 'multipart/form-data',
                data: data,
                success: function (response) {
                    fieldName = app.prepareKey(fieldName);

                    if (response) {
                        app.$data['formFields'][fieldName]['errorText'] = response[0];
                    } else {
                        app.$data['formFields'][fieldName]['errorText'] = null;
                    }
                }
            });
        },
        prepareKey: function (fieldName) {
            let symbPosition = fieldName.indexOf('_');
            if (symbPosition > 0) {
                fieldName = fieldName.replace("_", "");
                let upSymb = fieldName.charAt(symbPosition).toUpperCase();
                fieldName = fieldName.substr(0, symbPosition) + upSymb + fieldName.substr(symbPosition + upSymb.length);
            }

            return fieldName;
        }
    },
    watch: {
        formFields: {
            handler(val) {
                if ($("#registration").length == 1) {
                    $.each(val, function (fieldName, fieldValue) {
                        localStorage.setItem(fieldName, fieldValue.value == null ? "" : fieldValue.value);
                    });
                }
            },
            deep: true
        }
    },
    mounted() {
        if ($("#registration").length == 1) {
            var self = this;
            $.each(this.formFields, function (fieldName, fieldValue) {
                self.$data['formFields'][fieldName]['value'] = localStorage.getItem(fieldName);
            });
        }

        if (($("#id").length == 1) && ($("#update-member").length == 1)) {
            let memberId = $("#id").val();
            $.ajax({
                type: 'GET',
                url: '/admin/members/get-member/' + memberId,
                data: {
                    id: memberId
                },
                success: function (response) {
                    $.each(response, function (fieldName, fieldValue) {
                        if (fieldName != 'id' &&
                            fieldName != 'created_at' &&
                            fieldName != 'updated_at' &&
                            fieldName != 'status'
                        ) {
                            fieldName = app.prepareKey(fieldName);
                            app.$data['formFields'][fieldName]['value'] = fieldValue;
                        }
                    });
                }
            });
        }
    }
}).mount('#app');

const members = createApp({
    data() {
    },
    methods: {
        changeStatus: function (memberId) {
            $.ajax({
                type: 'GET',
                url: '/admin/members/change-status/' + memberId,
                data: {
                    id: memberId
                },
                success: function (response) {
                    if (response == 1) {
                        $("#status-" + memberId).addClass("fa fa-eye");
                        $("#status-" + memberId).removeClass("far fa-eye-slash");
                    } else {
                        $("#status-" + memberId).addClass("far fa-eye-slash");
                        $("#status-" + memberId).removeClass("fa fa-eye");
                    }
                }
            });
        },
        deleteMember: function (memberId) {
            $.ajax({
                type: 'POST',
                url: '/admin/member/delete/' + memberId,
                data: {
                    id: memberId,
                    _method: 'DELETE'
                },
                success: function (response) {
                    let el = document.getElementById('member-' + memberId);
                    el.remove();
                }
            });
        }
    }
}).mount('#members-table');

flatpickr("input[type=datetime-local]", {
    enableTime: true,
    dateFormat: "Y-m-d",
});

function phoneMask() {
    var num = $(this).val().replace(/\D/g, '');
    $(this).val('+' + num.substring(0, 1) + ' (' + num.substring(1, 4) + ') ' + num.substring(4, 7) + '-' + num.substring(7, 11));
    app.$data['formFields']['phone']['value'] = $(this).val();
}

$('[type="tel"]').keyup(phoneMask);
