<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->id()->unsigned();
            $table->string('first_name', 255);
            $table->string('last_name', 255);
            $table->date('birthday');
            $table->string('report_subject', 255);
            $table->string('country', 50);
            $table->string('phone', 17);
            $table->string('email', 255)->unique();
            $table->string('company', 255)->nullable();
            $table->string('position', 255)->nullable();
            $table->longText('about_me')->nullable();
            $table->string('photo', 33)->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
