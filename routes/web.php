<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/', [App\Http\Controllers\MemberController::class, 'index'])->name('index');
Route::get('/members', [App\Http\Controllers\MemberController::class, 'members'])->name('members');
Route::post('/check/{field}', [App\Http\Controllers\MemberController::class, 'check'])->name('check');
Route::post('/member/create', [App\Http\Controllers\MemberController::class, 'create'])
    ->name('member-create');

Route::prefix('admin')->middleware('auth')->group(function () {
    Route::get('/members/get-member/{id}', [App\Http\Controllers\AdminController::class, 'getMember'])
        ->name('getMember');
    Route::get('/members/change-status/{id}', [App\Http\Controllers\AdminController::class, 'statusChange'])
        ->name('statusChange');
    Route::get('/members/edit/{id}', [App\Http\Controllers\AdminController::class, 'edit'])
        ->name('edit');
    Route::post('/member/update/{id}', [App\Http\Controllers\AdminController::class, 'update'])
        ->name('update');
    Route::delete('/member/delete/{id}', [App\Http\Controllers\AdminController::class, 'delete'])
        ->name('delete');
});
