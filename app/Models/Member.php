<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    use HasFactory;

    public const STATUS_ACTIVE = 1;
    public const STATUS_BLOCKED = 0;

    protected $fillable = [
        'first_name',
        'last_name',
        'birthday',
        'report_subject',
        'country',
        'phone',
        'email',
        'company',
        'position',
        'about_me',
    ];
}
