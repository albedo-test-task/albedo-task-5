<?php

namespace App\Http\Controllers;

use App\Http\Requests\MemberRequest;
use App\Models\Member;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;

class AdminController extends Controller
{
    /**
     * @param  int  $id
     * @return JsonResponse
     */
    public function statusChange(int $id): JsonResponse
    {
        $member = Member::findOrFail($id);
        $member->status = ($member->status) == 1 ? 0 : 1;
        $member->save();

        return response()->json($member->status);
    }

    /**
     * @param  int  $id
     * @return Application|\Illuminate\Contracts\View\Factory|View|void
     */
    public function edit(int $id)
    {
        return view('edit', [
            'id' => $id,
        ]);
    }

    /**
     * @param  MemberRequest  $request
     * @param  int  $id
     * @return JsonResponse
     */
    public function update(MemberRequest $request, int $id)
    {
        $member = Member::findOrFail($id);

        if (isset($request->validator) && $request->validator->fails()) {
            $messages = $request->validator->messages();
            $messages = json_decode(json_encode($messages), true);

            if (array_key_exists('email', $messages) && $member->email == $request->input('email')) {
                unset($messages['email']);
            }

            if (isset($messages) && count($messages)) {
                return response()->json(['validateErrors' => $messages]);
            }
        }
        $member->fill($request->all());

        if ($request->file('photo') && $request->file('photo')->isValid()) {
            $fileName = time() . rand(0, 100) . "." . $request->file('photo')->extension();
            $request->file('photo')->move('uploads/photos/', $fileName);
            $member->photo = 'uploads/photos/' . $fileName;
        }

        return response()->json($member->save());
    }

    /**
     * @param  int  $id
     * @return JsonResponse
     */
    public function getMember(int $id): JsonResponse
    {
        $member = Member::findOrFail($id);

        return response()->json($member);
    }

    /**
     * @param  int  $id
     * @return Application|\Illuminate\Contracts\View\Factory|View|void
     */
    public function delete(int $id)
    {
        $member = Member::findOrFail($id);
        return response()->json($member->delete());
    }
}
