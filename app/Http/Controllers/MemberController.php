<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use App\Models\Member;
use App\Http\Requests\MemberRequest;
use Illuminate\Support\Facades\Auth;

class MemberController extends Controller
{
    /**
     * @return void
     */
    public function index()
    {
        return view('index');
    }

    /**
     * @param  MemberRequest  $request
     * @param  string  $field
     * @return JsonResponse
     */
    public function check(MemberRequest $request, string $field): JsonResponse
    {
        if (isset($request->validator) && $request->validator->fails()) {
            $messages = json_decode($request->validator->messages());

            if (isset($messages->$field)) {
                return response()->json($messages->$field);
            }
        }

        return response()->json(false);
    }

    /**
     * @param  MemberRequest  $request
     * @return JsonResponse
     */
    public function create(MemberRequest $request): JsonResponse
    {
        if (isset($request->validator) && $request->validator->fails()) {
            $messages = json_decode($request->validator->messages());

            if (isset($messages)) {
                return response()->json(['validateErrors' => $messages]);
            }
        }

        $member = new Member($request->all());

        if ($request->file('photo') && $request->file('photo')->isValid()) {
            $fileName = time() . rand(0, 100) . "." . $request->file('photo')->extension();
            $request->file('photo')->move('uploads/photos/', $fileName);
            $member->photo = 'uploads/photos/' . $fileName;
        } else {
            $member->photo = 'default-files/no-image.jpg';
        }

        return response()->json($member->save());
    }

    public function members()
    {
        if (Auth::check()) {
            $members = Member::orderBy('id', 'desc')->get();
        } else {
            $members = Member::where('status', Member::STATUS_ACTIVE)
                ->orderBy('id', 'desc')
                ->get();
        }

        return view('members', [
            'members' => $members
        ]);
    }
}
