<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\NotNumbers;

class MemberRequest extends FormRequest
{
    public $validator = null;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => ['required', 'string', 'max:255', new NotNumbers],
            'last_name' => ['required', 'string', 'max:255', new NotNumbers],
            'birthday' => ['required', 'date_format:Y-m-d', 'before_or_equal:now'],
            'report_subject' => ['required', 'string', 'max:255'],
            'country' => ['required', 'string', 'max:50'],
            'email' => ['required', 'email', 'max:255', 'unique:members'],
            'phone' => ['required', 'regex:/^\+\d\s\(\d{3}\)\s\d{3}\-\d{4}$/'],
            'company' => ['nullable', 'max:255'],
            'position' => ['nullable', 'max:255'],
            'photo' => ['nullable', 'image', 'mimes:jpeg,png,jpg,gif', 'max:2048'],
            'about_me' => ['nullable'],
        ];
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $this->validator = $validator;
    }
}
